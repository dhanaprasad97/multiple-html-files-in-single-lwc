import {LightningElement, api} from 'lwc';
import optionA from './optionA.html';
import optionB from './optionB.html';


export default class MultipleHtmlTemplatesExample extends LightningElement {

   @api displayOptionA = false;

   render(){
       //If displayOptionA is true return optionA template
       return this.displayOptionA ? optionA : optionB;
   }

   toggleOptions(){
       this.displayOptionA = !this.displayOptionA;
   }

}