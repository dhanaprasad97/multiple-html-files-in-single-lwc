# Multiple HTML files in a component bundle LWC #

When working with lightning web components there may be times when you want to render a component with different looks that are dependent on the data passed in. Instead of mixing all the different styles in one file, we can have multiple HTML templates in the same component and render them conditionally.

